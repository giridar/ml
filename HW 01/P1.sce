function E = f(X, Y, W)
    dim = size(X);
    E = 0;
    for i=1:dim(1)
        E = E + max(0, -Y(i) * (W * X(i,:)'));
    end
endfunction

X = [-1*sin(-1) 1; -1*sin(1) 1; 1*sin(-1) 1; 1*sin(1) 1];
Y = [1; -1; -1; 1];
dim = size(X);
n = dim(1);
m = dim(2);
W = zeros(n+1, m);
g = 1;

for t=1:n
    dfw = zeros(1, m);
    for i=1:n
        if -Y(i) * (W(t,:) * X(i,:)') >= 0 then
            dfw = dfw - Y(i) * X(i,:);
        end
    end
    dfw
    W(t+1,:) = W(t,:) - g * dfw;
    if dfw == 0 then
        break
    end
end

W(2,:)
f(X, Y, W(2,:))
W(3,:)
f(X, Y, W(3,:))
W(4,:)
f(X, Y, W(4,:))
t-1
W(t,:)
f(X, Y, W(t,:))
t
W(t+1,:)
f(X, Y, W(t+1,:))
