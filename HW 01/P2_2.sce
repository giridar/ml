function E = f(X, Y, W, b)
    dim = size(X);
    E = 0;
    for i=1:dim(1)
        E = E + max(0, -Y(i) * (W * X(i,:)' + b));
    end
endfunction

XY = read('/home/giridar/Desktop/Collections/Studies/UTD/Fall 15/Machine Learning/HWs & Projs/HW 01/perceptron.data', -1, 5);
dim = size(XY);
n = dim(1);
m = dim(2)-1;
X = XY(:, 1:m);
Y = XY(:, m+1);
W = zeros(n+1, m);
b = zeros(n+1, 1);
g = 1;

for t=1:10*n
    dfw = zeros(1, m);
    dfb = 0;
    //index cannot be 0
    k = modulo(t, n);
    if k == 0 then
        k = n;
    end
    if -Y(k) * (W(t,:) * X(k,:)' + b(t)) >= 0 then
        dfw = dfw - Y(k) * X(k,:);
        dfb = dfb - Y(k);
    end
    W(t+1,:) = W(t,:) - g * dfw;
    b(t+1) = b(t) - g * dfb;
    if f(X, Y, W(t+1,:), b(t+1)) == 0 then
        break
    end
end

W(2,:), b(2)
W(3,:), b(3)
W(4,:), b(4)
W(102,:), b(102)
W(202,:), b(202)
W(302,:), b(302)
t
W(t+1,:), b(t+1)
