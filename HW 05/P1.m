X = importdata('zoo.data');
[n, m] = size(X);
L1 = 0;
W = zeros(m, m);
for i = 1:m
    Ui = unique(X(:,i));
    Ni = histc(X(:,i), Ui);

    L1 = L1 + sum(Ni .* log(Ni));

    Xi10 = X(:,i) * 10;
    for j = 1:i-1
        Uj = unique(X(:,j));
        Nj = histc(X(:,j), Uj);
        Xij = Xi10 + X(:,j);
        Uij = unique(Xij);
        Nij = histc(Xij, Uij);

        w = 0;
        for xi = Ui'
            ni = Ni(Ui == xi);
            for xj = Uj'
                nj = Nj(Uj == xj);
                nij = Nij(Uij == (xi * 10 + xj));
                if ~isempty(nij)
                    w = w + nij * log((nij*n) / (ni*nj));
                end
            end
        end
        W(i,j) = w/n;
    end
end

maxW = max(W(:)) + 1;
UG = tril(sparse(maxW - W));
root = 17;
[MST, parents] = graphminspantree(UG, root);

w = [];
for i = 1:m
    if parents(i) == 0
        w = [w, 0];
    elseif ~isempty(nonzeros(MST(parents(i), i)))
        w = [w, nonzeros(MST(parents(i), i))];
    else
        w = [w, nonzeros(MST(i, parents(i)))];
    end
end
w = maxW - w;
L2 = sum(w);
MaximumLikelihood = L1 + L2
parents(parents == 0) = root;
DirectedMaximumSpanningTree = sparse(1:m, parents, w)
view(biograph(DirectedMaximumSpanningTree,[],'ShowArrows','on','ShowWeights','on'));