'''
Created on Nov 5, 2015

@author: giridar
'''
import CNB
from _csv import reader
import NB

def getData(fname):
    data = []
    with open(fname, 'r') as f:
        fileReader = reader(f, delimiter=',')
        for line in fileReader:
            item = []
            for x in line:
                if x == 'democrat':
                    item.append(0)
                elif x == 'republican':
                    item.append(1)
                elif x == 'y':
                    item.append(0)
                elif x == 'n':
                    item.append(1)
                else:
                    item.append(2)
            data.append(item)
    return data

if __name__ == '__main__':
    trainDataFile = 'voting_train.data'
    testDataFile = 'voting_test.data'

    data = getData(trainDataFile)
    p_y, p_xy = CNB.learn(data)
    data = getData(testDataFile)
    p, accuracy = CNB.predict(data, p_y, p_xy)
    print accuracy
    data = NB.delete(getData(testDataFile))
    p, accuracy = CNB.predict(data, p_y, p_xy)
    print accuracy

    data = NB.delete(getData(trainDataFile))
    p_y, p_xy = CNB.learn(data)
    data = NB.delete(getData(testDataFile))
    p, accuracy = CNB.predict(data, p_y, p_xy)
    print accuracy

    data = NB.impute(getData(trainDataFile))
    p_y, p_xy = CNB.learn(data)
    data = NB.delete(getData(testDataFile))
    p, accuracy = CNB.predict(data, p_y, p_xy)
    print accuracy
    pass
