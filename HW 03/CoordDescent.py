'''
Created on Oct 20, 2015

@author: giridar
'''
from math import exp, log
from Input import INDEX
from DecisionTree import getLabel
from copy import deepcopy
from decimal import Decimal
from random import randint

def expLoss(X, Y, H, a):
    return sum([exp(-xi[Y[INDEX]] * sum([(a[t] * getLabel(xi, H[t])) for t in range(len(H))])) for xi in X])

def learn(X, Y, H):
    lm = len(H)
    a = [0] * lm
    
    prev_loss = round(Decimal(expLoss(X, Y, H, a)), 3)
    while True:
        updated = False
        for t in range(lm):
            """t = randint(0, lm-1)"""
            Hp = deepcopy(H)
            ht = Hp.pop(t)
            ap = deepcopy(a)
            at = ap.pop(t)
            Xa = []
            Xe = []
            for xi in X:
                if getLabel(xi, ht) == xi[Y[INDEX]]:
                    Xa += [xi]
                else:
                    Xe += [xi]
            atp = 0.5 * log(expLoss(Xa, Y, Hp, ap) / expLoss(Xe, Y, Hp, ap))
            if atp != at:
                a[t] = atp
                updated = True

        loss = round(Decimal(expLoss(X, Y, H, a)), 3)
        if (not updated) or (loss == prev_loss):
            break
        prev_loss = loss
        print loss
    return a
