X = [0 0 1 1; -1 -1 -1 1; 1 1 -2 1];
Y = [1; 1; -1];
b = [-1; -1; -1];
dim = size(X);
n = dim(1);
m = dim(2);
c = zeros(n,n);
w = zeros(1,m);

for i=1:n
    for j=1:n
        c(i,j) = Y(j) * Y(i) * X(j,:) * X(i,:)';
    end
end
c

l = linsolve(c, b)'

for i=1:n
    w = w + l(i) * Y(i) * X(i,:);
end
w
1/norm(w)
