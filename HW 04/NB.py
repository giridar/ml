'''
Created on Nov 5, 2015

@author: giridar
'''
import copy
import CNB

def delete(data):
    new = []
    for item in data:
        missing = False
        for x in item:
            if x > 1:
                missing = True
        if not missing:
            new.append(copy.deepcopy(item))
    return new

def impute(data):
    _, n_xy = CNB.count(data)
    mode = []
    for i in range(16):
        n0 = n_xy[i][0][0] + n_xy[i][1][0]
        n1 = n_xy[i][0][1] + n_xy[i][1][1]
        if n0 > n1:
            mode.append(0)
        else:
            mode.append(1)
    new = []
    for item in data:
        new_item = []
        for j in range(17):
            if item[j] < 2:
                new_item.append(item[j])
            else:
                new_item.append(mode[j-1])
        new.append(new_item)
    return new
