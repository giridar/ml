'''
Created on Nov 5, 2015

@author: giridar
'''

def count(data):
    n_y = [0, 0]
    n_xy = []
    for _ in range(16):
        n_xy.append([[0, 0], [0, 0]])
    for item in data:
        l = item[0]
        n_y[l] += 1
        for j in range(16):
            k = item[j + 1]
            if k < 2:
                n_xy[j][l][k] += 1
    return n_y, n_xy

def learn(data):
    n_y, n_xy = count(data)
    n = n_y[0] + n_y[1]
    p_y = [float(n_y[0]) / n, float(n_y[1]) / n]
    p_xy = []
    for i in range(16):
        p_c = []
        for j in range(2):
            nj = n_xy[i][j][0] + n_xy[i][j][1]
            p_c.append([float(n_xy[i][j][0]) / nj, float(n_xy[i][j][1]) / nj])
        p_xy.append(p_c)
    return p_y, p_xy

def predict(data, p_y, p_xy):
    matches = 0
    prediction = []
    for i in range(len(data)):
        prediction.append(getLabel(data[i], p_y, p_xy))
        label = data[i][0]
        if prediction[i] == label:
            matches += 1
    return prediction, (float(matches) / len(prediction)) * 100

def getLabel(x, p_y, p_xy):
    p0 = p_y[0]
    p1 = p_y[1]
    for j in range(16):
        if x[j] < 2:
            p0 *= p_xy[j][0][x[j]]
            p1 *= p_xy[j][1][x[j]]
    if p0 > p1:
        return 0
    return 1
