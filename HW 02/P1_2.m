trainDataFile = 'spambase_train.data';
validationDataFile = 'spambase_validation.data';
testDataFile = 'spambase_test.data';
c = [1; 10; 100; 1000; 10000];
s = [.001; .01; .1; 1; 10; 100];
accuracy = zeros(size(c,1), size(s,1), 3);
best_c_idx = 1;
best_s_idx = 1;
for j=1:size(c)
    for i=1:size(s)
        [x, y] = svm_data(trainDataFile);
        [l_sv, x_sv, y_sv, b] = dual_learn(x, y, c(j), s(i));
        [p, accuracy(j, i, 1)] = dual_predict(x, y, l_sv, x_sv, y_sv, b, s(i));
        
        [x, y] = svm_data(validationDataFile);
        [p, accuracy(j, i, 2)] = dual_predict(x, y, l_sv, x_sv, y_sv, b, s(i));
        if (accuracy(j, i, 2) >= accuracy(best_c_idx, best_s_idx, 2))
            best_c_idx = j;
            best_s_idx = i;
        end
        
        [x, y] = svm_data(testDataFile);
        [p, accuracy(j, i, 3)] = dual_predict(x, y, l_sv, x_sv, y_sv, b, s(i));
    end
end
accuracy, c(best_c_idx), s(best_s_idx), accuracy(best_c_idx, best_s_idx, 2)
