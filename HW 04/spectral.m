function C = spectral(x, s, k)
%Spectral clustering
%Input:
%       x -> set of n points (n x m)
%       s -> sigma value to use in pair-wise similarity matrix calculation 
%       k -> no. of desired clusters
%Output:
%       C -> list of cluster indices {1:k} for the given points (n x 1)
n = size(x, 1);
A = zeros(n, n);
for i = 1:n
    for j = i:n
        A(i, j) = exp( -norm(x(i,:) - x(j,:))^2 / (2 * s^2));
        A(j, i) = A(i, j);
    end
end
D = diag(sum(A, 2));
L = D - A;
[v, l] = eig(L);
[~, li] = sort(diag(l));
V = v(:, li(1:k));
C = kmeans(V, k);
end