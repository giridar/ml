'''
Created on Oct 17, 2015

@author: giridar
'''
from itertools import product
from Input import INDEX, NAME, VALUES
from math import log
from copy import deepcopy

def learn(data, attributes, target):
    labels = [sample[target[INDEX]] for sample in data]
    labelSet = set(labels)
    if len(labelSet) == 1:
        return labelSet.pop()

    majority = max([(labels.count(label), label) for label in labelSet])[1]
    if len(attributes) == 0:
        return majority

    best = None
    if len(attributes) == 1:
        best = attributes[0]
    else:
        cE = [(conditionalEntropy(data, attr, target), attr) for attr in attributes]
        best = min(cE)[1]
    node = (best[NAME], best[INDEX])
    tree = {node: {}}
    for value in best[VALUES]:
        partitioin = [sample for sample in data if sample[best[INDEX]] == value]
        if len(partitioin) == 0:
            subtree = majority
        else:
            subTreeAttributes = deepcopy(attributes)
            subTreeAttributes.remove(best)
            subtree = learn(partitioin, subTreeAttributes, target)
        tree[node][value] = subtree
    return tree

def conditionalEntropy(data, attr, target):
    values = [sample[attr[INDEX]] for sample in data]
    pX = [(value, float(values.count(value)) / len(values)) for value in set(values)]
    return sum([px[1] * entropy([sample for sample in data if sample[attr[INDEX]] == px[0]], target) for px in pX])

def entropy(data, attr):
    labels = [sample[attr[INDEX]] for sample in data]
    pY = [float(labels.count(label)) / len(labels) for label in set(labels)]
    return -sum([py * log(py, 2) for py in pY])

def depth(tree):
    if not isinstance(tree, dict):
        return 0;
    node = tree.items()[0]
    return max([depth(subTree) for subTree in node[1].values()]) + 1

def predict(data, tree, target):
    matches = 0
    prediction = []
    for i in range(len(data)):
        prediction += [getLabel(data[i], tree)]
        label = data[i][target[INDEX]]
        if prediction[i] == label:
            matches += 1
    return prediction, (float(matches) / len(prediction)) * 100

def getLabel(sample, tree):
    if isinstance(tree, dict):
        node = tree.items()[0]
        key = node[0]
        value = sample[key[1]]
        subTrees = node[1]
        return getLabel(sample, subTrees[value])
    return tree

def genAllTrees(attributes, labels, depth):
    trees = [{}]
    while getEmptyNode(trees[0], depth) is not None:
        trees = assignEmptyNode(attributes, labels, depth, trees)
    return trees

def assignEmptyNode(attributes, labels, depth, trees):
    newTrees = []
    for tree in trees:
        cNode, usedAttrs = getEmptyNode(tree, depth)
        if len(usedAttrs) == depth:
            cAttr = usedAttrs[len(usedAttrs) - 1]
            for label in product(labels, repeat=len(cNode[cAttr])):
                cTree = deepcopy(tree)
                cNode, usedAttrs = getEmptyNode(cTree, depth)
                i = 0
                for item in cNode[cAttr].items():
                    cNode[cAttr][item[0]] = label[i]
                    i += 1
                newTrees += [cTree]

        else:
            for attr in attributes:
                key = (attr[NAME], attr[INDEX])
                if key not in usedAttrs:
                    cTree = deepcopy(tree)
                    cNode, usedAttrs = getEmptyNode(cTree, depth)
                    cNode[key] = {}
                    for value in attr[VALUES]:
                        cNode[key][value] = {}
                    newTrees += [cTree]
    return newTrees

def getEmptyNode(tree, depth, usedAttrs=[]):
    if not isinstance(tree, dict):
        return None

    if len(tree) == 0:
        return tree, usedAttrs
    
    node = tree.items()[0]
    usedAttrs = deepcopy(usedAttrs) + [node[0]]
    subTrees = node[1]
    for subTree in subTrees.values():
        result = getEmptyNode(subTree, depth, usedAttrs)
        if result is not None:
            if len(usedAttrs) == depth:
                return tree, usedAttrs
            return result
    return None
