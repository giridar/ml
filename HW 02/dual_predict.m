function [z, accuracy] = dual_predict(x, y, l_sv, x_sv, y_sv, b, s)
n = size(x, 2);
n_sv = size(x_sv, 2);
z = zeros(1, n);
for i=1:n
    Wx = 0;
    for j=1:n_sv
        Wx = Wx + (l_sv(j) * y_sv(j) * exp(-norm(x_sv(:,j)-x(:,i))^2 / (2 * s^2)));
    end
    z(i) = sign(Wx + b);
end
accuracy = (length(find(y.*z >= 1)) / n) * 100;
end