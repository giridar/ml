function E = f(X, Y, W, b)
    dim = size(X);
    E = 0;
    for i=1:dim(2)
        E = E + max(0, -Y(i) * (W' * X(:,i) + b));
    end
endfunction

XY = read('/home/giridar/Desktop/Collections/Studies/UTD/Fall 15/Machine Learning/HWs & Projs/HW 01/perceptron.data', -1, 5);
dim = size(XY);
n = dim(1);
m = dim(2)-1;
X = XY(:, 1:m)';
Y = XY(:, m+1)';
W = zeros(m, n+1);
b = zeros(1, n+1);
g = 1;

for t=1:10*n
    dfw = zeros(m, 1);
    dfb = 0;
    for i=1:n
       if -Y(i) * (W(:,t)' * X(:,i) + b(t)) >= 0
            dfw = dfw - Y(i) * X(:,i);
            dfb = dfb - Y(i);
       end
    end
    W(:,t+1) = W(:,t) - g * dfw;
    b(t+1) = b(t) - g * dfb;
    if f(X, Y, W(:,t+1), b(t+1)) == 0 then
        break
    end
end

W(:,2), b(2)
W(:,3), b(3)
W(:,4), b(4)
t
W(:,t+1), b(t+1)
