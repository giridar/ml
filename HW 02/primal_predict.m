function [z, accuracy] = primal_predict(x, y, W, b)
n = size(x, 2);
z = sign((W' * x) + b);
accuracy = (length(find(y.*z >= 1)) / n) * 100;
end
