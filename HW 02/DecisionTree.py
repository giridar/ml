'''
Created on Sep 24, 2015

@author: giridar
'''
import math
import csv

def dTreeLearn(data, attributes, target):
    labels = [sample[target['index']] for sample in data]
    labelSet = set(labels)
    if len(labelSet) == 1:
        return labelSet.pop()

    majority = max([(labels.count(label), label) for label in labelSet])[1]
    if len(attributes) == 0:
        return majority

    best = None
    if len(attributes) == 1:
        best = attributes[0]
    else:
        cE = [(conditionalEntropy(data, attr, target), attr) for attr in attributes]
        best = min(cE)[1]
    node = (best['name'], best['index'])
    tree = {node: {}}
    for value in best['values']:
        partitioin = [sample for sample in data if sample[best['index']] == value]
        if len(partitioin) == 0:
            subtree = majority
        else:
            subTreeAttributes = list(attributes)
            subTreeAttributes.remove(best)
            subtree = dTreeLearn(partitioin, subTreeAttributes, target)
        tree[node][value] = subtree
    return tree

def conditionalEntropy(data, attr, target):
    values = [sample[attr['index']] for sample in data]
    pX = [(value, float(values.count(value)) / len(values)) for value in set(values)]
    return sum([px[1] * entropy([sample for sample in data if sample[attr['index']] == px[0]], target) for px in pX])

def entropy(data, attr):
    labels = [sample[attr['index']] for sample in data]
    pY = [float(labels.count(label)) / len(labels) for label in set(labels)]
    return -sum([py * math.log(py, 2) for py in pY])

def depth(dTree):
    if not isinstance(dTree, dict):
        return 0;
    node = dTree.items()[0]
    return max([depth(subTree) for subTree in node[1].values()]) + 1

def dTreePredict(data, dTree):
    matches = 0
    prediction = []
    for i in range(len(data)):
        prediction.append(dTreeGetLabel(data[i], dTree))
        label = data[i][target['index']]
        if prediction[i] == label:
            matches += 1
    return prediction, (float(matches) / len(prediction)) * 100

def dTreeGetLabel(sample, dTree):
    if isinstance(dTree, dict):
        node = dTree.items()[0]
        key = node[0]
        value = sample[key[1]]
        subTrees = node[1]
        return dTreeGetLabel(sample, subTrees[value])
    return dTree

def getKeys(fname):
    with open(fname, 'r') as f:
        keys = []
        lines = f.read().splitlines()
        for i in range(len(lines)):
            tokens = lines[i].split()
            values = tokens[1].split(',')
            key = {}
            key['index'] = i
            key['values'] = [value.split('=')[1] for value in values]
            key['name'] = tokens[0].strip(':')
            keys.append(key)
        return keys

def getData(fname):
    with open(fname, 'r') as f:
        fileReader = csv.reader(f, delimiter=',')
        return [line for line in fileReader]

if __name__ == '__main__':
    trainDataFile = 'mush_train.data'
    testDataFile = 'mush_test.data'
    attrKeyFile = 'attributes.key'

    attributes = getKeys(attrKeyFile)
    print attributes
    target = attributes.pop(0)

    data = getData(trainDataFile)
    dTree = dTreeLearn(data, attributes, target)
    print dTree
    print depth(dTree)
    print dTreePredict(data, dTree)[1]
    data = getData(testDataFile)
    print dTreePredict(data, dTree)[1]
    pass
