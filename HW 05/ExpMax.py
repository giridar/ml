'''
Created on Nov 17, 2015

@author: giridar
'''
import math
import numpy as np
import JointProb

def likelihood(pX, pM, data):
    L = 0
    for x in data:
        Lx = math.log(pX.prob(x))
        Lp = sum([math.log(pM[j][0]) if xv == '?' else math.log(pM[j][1]) for j, xv in enumerate(x)])
        L += Lx + Lp 
        '''values = [[xj] if isinstance(xj, int) else range(pX.d[j]) for j, xj in enumerate(x)]
        for xv in itertools.product(*values):
            q = pX.condProb(xv, x)
            L += q * math.log(pX.prob(xv) / q)'''
    return L

def learnMissingness(data):
    m = len(data[0])
    pM = np.zeros([m, 2])
    for x in data:
        for j, xv in enumerate(x):
            if xv == '?':
                pM[j][0] += 1.0
            else:
                pM[j][1] += 1.0
                
    for j in xrange(m):
        pM[j] = JointProb.normalize(pM[j])
    return pM

def expMax(data, pX_0, pM):
    n = len(data)
    pX = pX_0
    Lo = float('-inf')
    Ln = likelihood(pX, pM, data)
    L0 = Ln
    while(Ln > Lo):
        Lo = Ln
        cX = np.zeros(pX.p.shape)
        for x in data:
            cX = np.add(cX, pX.condJointDist(x).p)
        pX = JointProb.getFrom(np.divide(cX, n))
        Ln = likelihood(pX, pM, data)
    print L0, Ln
    return pX
