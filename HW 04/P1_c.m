p = imread('bw.jpg');
[xl, yl] = size(p);
p = reshape(im2double(p), [], 1);
n = size(p, 1);
x = zeros(n, 1);
y = zeros(n, 1);
for i = 1:n
   x(i) = ceil(i/xl);
   y(i) = mod(i, xl);
   if y(i) == 0
       y(i) = xl;
   end
   y(i) = xl + 1 - y(i);
end
sl = [.001 .01 .1 1 10 100];
k = 2;
figure('Name', 'P1_c: Pixel Clustering');
subplot(4, 2, 1);
scatter(x, y, [], p, 'filled');
title('Original');
axis equal;
subplot(4, 2, 2);
scatter(x, y, [], kmeans(p, k), 'filled');
title('kMeans');
axis equal;
for i = 1:size(sl, 2)
    subplot(4, 2, i+2);
    scatter(x, y, [], spectral(p, sl(i), k), 'filled');
    title(strcat('Spectral: ', num2str(sl(i))));
    axis equal;
end