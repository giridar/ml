'''
Created on Nov 16, 2015

@author: giridar
'''
from _csv import reader
import JointProb
from ExpMax import expMax, learnMissingness
import numpy as np

def getData(fname):
    data = []
    with open(fname, 'r') as f:
        fileReader = reader(f, delimiter=',')
        for line in fileReader:
            item = []
            for x in line:
                if x.isdigit():
                    item += [int(x)]
                else:
                    item += [x]
            data.append(item)
    return data

def genAttribute(pX):
    r = np.random.random()
    cP = 0
    for xj, px in enumerate(JointProb.normalize(pX)):
        cP += px
        if r <= cP:
            return xj
    return None

def genData(pX, pM, model):
        m = len(model)
        if m == len(pX):
            x = ['?'] * m
            while '?' in x:
                for j, parents in enumerate(model):
                    if x[j] == '?':
                        if parents == None:
                            x[j] = genAttribute(pX[j])
                    
                        else:
                            if all([x[parent] != '?' for parent in parents]):
                                xp = [x[parent] for parent in parents]
                                '''i = [[ij] for ij in xp]'''
                                if len(xp) == 1:
                                    x[j] = genAttribute(pX[j][xp[0]])
                                else:
                                    x[j] = genAttribute(pX[j][xp[0]][xp[0]])

            for j in xrange(m):
                if np.random.random() <= pM[j][0]:
                    x[j] = '?'

            return x
        return None

if __name__ == '__main__':
    trainDataFile = 'bn.data'
    data = getData(trainDataFile)
    d = [2] * len(data[0])
    model = [None, [0], [0], [1, 2]]

    pX_0 = JointProb.JointProb(d)
    pX_0.learn(data)
    pM = learnMissingness(data)
    print pM

    pX = expMax(data, pX_0, pM)
    pX = pX.factorize(model)
    print pX

    expMax(data, JointProb.getUniform(d), pM)

    for i in xrange(10):
        expMax(data, JointProb.getFrom(np.random.random(d)), pM)

    for i in xrange(10):
        print genData(pX, pM, model)    
    pass
