'''
Created on Oct 17, 2015

@author: giridar
'''
from _csv import reader

INDEX = 'index'
NAME = 'name'
VALUES = 'values'

def getKeys(fname):
    with open(fname, 'r') as f:
        keys = []
        lines = f.read().splitlines()
        for i in range(len(lines)):
            tokens = lines[i].split()
            values = tokens[1].split(',')
            key = {}
            key[INDEX] = i
            key[NAME] = tokens[0].strip(':')
            key[VALUES] = [sign(value) for value in values]
            keys += [key]
        return keys

def getData(fname):
    with open(fname, 'r') as f:
        fileReader = reader(f, delimiter=',')
        return [[sign(x) for x in line] for line in fileReader]

def sign(data):
    if int(data) > 0:
        return 1
    return -1
