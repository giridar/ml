function y=f(x)
    y=max(x^2, (x+1)^2)
endfunction
function dy=g(x)
    if x > -1/2 then
        dy=2*(x+1)
    else 
        dy=2*x
    end
endfunction
x=linspace(-1, 1, 101);
plot(x,f,"b",x,g,"r")
