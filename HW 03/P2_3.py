'''
Created on Oct 20, 2015

@author: giridar
'''
import Input, DecisionTree, AdaBoost, CoordDescent

if __name__ == '__main__':
    trainDataFile = 'heart_train.data'
    testDataFile = 'heart_test.data'
    attrKeyFile = 'attributes.key'

    attributes = Input.getKeys(attrKeyFile)
    target = attributes.pop(0)

    depth = 1
    allTrees = DecisionTree.genAllTrees(attributes, target[Input.VALUES], depth)
    print allTrees
    """data = Input.getData(trainDataFile)
    a = CoordDescent.learn(data, target, allTrees)
    print a
    print CoordDescent.expLoss(data, target, allTrees, a)

    data = Input.getData(testDataFile)
    print AdaBoost.predict(data, target, allTrees, a)[1]"""

    M = 20
    data = Input.getData(trainDataFile)
    la = AdaBoost.learn(data, target, allTrees, [M])
    a = la[0]
    print a
    print CoordDescent.expLoss(data, target, allTrees, a)
    data = Input.getData(testDataFile)
    print AdaBoost.predict(data, target, allTrees, a)[1]
    pass
