'''
Created on Oct 19, 2015

@author: giridar
'''
import Input, DecisionTree, AdaBoost

if __name__ == '__main__':
    trainDataFile = 'heart_train.data'
    testDataFile = 'heart_test.data'
    attrKeyFile = 'attributes.key'

    attributes = Input.getKeys(attrKeyFile)
    target = attributes.pop(0)

    depth = 2
    allTrees = DecisionTree.genAllTrees(attributes, target[Input.VALUES], depth)
    data = Input.getData(trainDataFile)
    
    lM = [5, 10, 20, 50, 100]
    la = AdaBoost.learn(data, target, allTrees, lM)

    data = Input.getData(testDataFile)
    print [AdaBoost.predict(data, target, allTrees, la[m])[1] for m in range(len(lM))]
    pass
