'''
Created on Nov 18, 2015

@author: giridar
'''
import numpy as np
import itertools

class JointProb:

    def __init__(self, d):
        self.d = d
        self.p = np.zeros(d)

    def learn(self, data):
        for x in data:
            if '?' not in x:
                self.p[self.given(x)] += 1.0
        self.p = normalize(self.p)

    def given(self, x):
        return [[xj] if isinstance(xj, int) else slice(self.d[j]) for j, xj in enumerate(x)]

    def prob(self, x):
        return np.sum(self.p[self.given(x)])

    def condJointDist(self, x):
        cP = JointProb(self.d)
        cP.p[self.given(x)] = self.p[self.given(x)]
        cP.p = normalize(cP.p)
        return cP

    def marginalDist(self, x):
        dim = [j for j in xrange(len(x)) if x[j] == '*']
        return np.sum(self.p, tuple(dim))

    def condProb(self, xm, xo):
        x1 = np.array(xm)
        x2 = np.array(xo)
        x = np.where(x1 == '?', x2, x1).astype(int)
        return self.condJointDist(xo).prob(x)

    def factorize(self, model):
        m = len(model)
        if m == len(self.d):
            p = []
            for j, parents in enumerate(model):
                if parents == None:
                    x = ['*'] * m
                    x[j] = '?'
                    pj = self.marginalDist(x)
                    '''pj = np.zeros(self.d[j])
                    x = ['?'] * m
                    for k in xrange(self.d[j]):
                        x[j] = k
                        pj[k] = self.prob(x)'''
                else:
                    dj = [self.d[k] for k in parents]
                    dj += [self.d[j]]
                    pj = np.zeros(dj)
                    p_values = [range(dj[k]) for k in xrange(len(parents))]
                    '''values = range(self.d[j])'''
                    for pv in itertools.product(*p_values):
                        x1 = ['?'] * m
                        for k, parent in enumerate(parents):
                            x1[parent] = pv[k]
                        cjd = self.condJointDist(x1)
                        x2 = ['*'] * m
                        x2[j] = '?'
                        pj[pv] = normalize(cjd.marginalDist(x2))
                        '''pj[list(pv)] = normalize(pj[list(pv)])'''
                    
                p += [pj]
            return p
        return None

    def __str__(self):
        return str(self.p)

def normalize(a):
    n = np.sum(a)
    a = np.divide(a, n)
    return a

def getFrom(a):
    if type(a).__module__ == np.__name__:
        pX = JointProb(a.shape)
        pX.p = a.copy()
        pX.p = normalize(pX.p)
        return pX
    return None

def getUniform(d):
    return getFrom(np.ones(d))
