'''
Created on Oct 19, 2015

@author: giridar
'''
from Input import INDEX
from DecisionTree import getLabel
from math import log, exp, sqrt, copysign
from copy import deepcopy

def weightedError(X, Y, h, w):
    return sum([w[i] for i in range(len(X)) if getLabel(X[i], h) != X[i][Y[INDEX]]])

def learn(X, Y, H, lm):
    M = len(H)
    a = [0] * M
    la = []
    N = len(X)
    w = [1.0 / N] * N
    for m in range(max(lm)):
        print m
        em, t = min([(weightedError(X, Y, H[t], w), t) for t in range(M)])
        am = 0.5 * log((1 - em) / em)
        for i in range(N):
            w[i] = (w[i] * exp(-X[i][Y[INDEX]] * am * getLabel(X[i], H[t]))) / (2 * sqrt(em * (1 - em)))
        a[t] = am
        if m < 5:
            print t, em, H[t]
        if m+1 in lm:
            la += [deepcopy(a)]
    return la

def predict(X, Y, H, a):
    matches = 0
    prediction = []
    for i in range(len(X)):
        prediction += [ copysign(1, sum([(a[m] * getLabel(X[i], H[m])) for m in range(len(H))])) ]
        if prediction[i] == X[i][Y[INDEX]]:
            matches += 1
    return prediction, (float(matches) / len(prediction)) * 100
