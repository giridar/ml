trainDataFile = 'spambase_train.data';
validationDataFile = 'spambase_validation.data';
testDataFile = 'spambase_test.data';
c = [1; 10; 100; 1000; 10000];
accuracy = zeros(size(c,1), 3);
best_c_idx = 1;
for i=1:size(c)
    [x, y] = svm_data(trainDataFile);
    [W, b, E] = primal_learn(x, y, c(i));
    [z, accuracy(i, 1)] = primal_predict(x, y, W, b);
    
    [x, y] = svm_data(validationDataFile);
    [z, accuracy(i, 2)] = primal_predict(x, y, W, b);
    if (accuracy(i, 2) >= accuracy(best_c_idx, 2))
        best_c_idx = i;
    end
    
    [x, y] = svm_data(testDataFile);
    [z, accuracy(i, 3)] = primal_predict(x, y, W, b);
end
accuracy, c(best_c_idx), accuracy(best_c_idx, 2)
