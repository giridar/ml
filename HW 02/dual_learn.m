function [l_sv, x_sv, y_sv, b] = dual_learn(x, y, c, s)
[m, n] = size(x);
H = zeros(n, n);
for i=1:n
    for j=i:n
        H(i,j) = y(i) * y(j) * exp(-norm(x(:,i)-x(:,j))^2 / (2 * s^2));
        H(j,i) = H(i,j);
    end
end
f = -ones(n, 1);
Aeq = y;
beq = 0;
lb = zeros(n, 1);
ub = c * ones(n, 1);
l = quadprog(H, f, [], [], Aeq, beq, lb, ub);

sv = find(l>0 & l<c);
if (isempty(sv))
    b = 0;
else
    b = sum(y(sv) - (l' .* y * H(:, sv))) / length(sv);
end
l_sv = l(sv);
x_sv = x(:,sv);
y_sv = y(sv);
end