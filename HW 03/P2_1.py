'''
Created on Oct 17, 2015

@author: giridar
'''
import DecisionTree, Input

if __name__ == '__main__':
    trainDataFile = 'heart_train.data'
    testDataFile = 'heart_test.data'
    attrKeyFile = 'attributes.key'

    attributes = Input.getKeys(attrKeyFile)
    target = attributes.pop(0)

    data = Input.getData(trainDataFile)
    tree = DecisionTree.learn(data, attributes, target)
    print tree
    print DecisionTree.depth(tree)
    print DecisionTree.predict(data, tree, target)[1]

    data = Input.getData(testDataFile)
    print DecisionTree.predict(data, tree, target)[1]
    pass
