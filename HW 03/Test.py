'''
Created on Oct 19, 2015

@author: gxe150130
'''
import math
from DecisionTree import genAllTrees
from math import exp

if __name__ == '__main__':
    a = [{'index':0, 'name':'a', 'values':[0, 1]}, {'index':1, 'name':'b', 'values':[0, 1]}, {'index':2, 'name':'c', 'values':[0, 1]}]
    l = [0, 1]
    t = genAllTrees(a, l, 2)
    print t
    print len(t)
    
    print 1.0 / 10
    print math.copysign(1, 1.0 / 10)
    print -1 / 10
    print math.copysign(1, -1 / 10)
    
    m = [5, 10, 20, 50, 100]
    print m[0:3]
    m1 = list(m)
    m1.remove(m[2])
    print m, m1
    m2 = list(m)
    m2.pop(2)
    print m, m2
    
    print exp(0)
    pass
