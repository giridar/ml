function [W, b, E] = primal_learn(x, y, c)
[m, n] = size(x);
xy = diag(y) * x';
H = diag([ones(1, m), zeros(1, n+1)]);
f = [zeros(m+1, 1); c * ones(n, 1)];
A = -[xy, y', eye(n)];
b = -ones(n, 1);
lb = [-inf(m+1, 1); zeros(n, 1)];
min_values = quadprog(H, f, A, b, [], [], lb, []);
W = min_values(1:m, :);
b = min_values(m+1, :);
E = min_values(m+2:end, :);
end
