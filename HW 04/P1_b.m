p = circs()';
sl = [.01 .1 1 10];
k = 2;
figure('Name', 'P1_b: Point Clustering');
subplot(3, 2, 1);
scatter(p(:, 1), p(:, 2));
title('Original');
axis equal;
subplot(3, 2, 2);
scatter(p(:, 1), p(:, 2), [], kmeans(p, k));
title('kMeans');
axis equal;
for i = 1:size(sl, 2)
    subplot(3, 2, i+2);
    scatter(p(:, 1), p(:, 2), [], spectral(p, sl(i), k));
    title(strcat('Spectral: ', num2str(sl(i))));
    axis equal;
end