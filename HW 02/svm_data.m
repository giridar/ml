function [x, y] = svm_data(file)
data = dlmread(file);
m = size(data,2)-1;
x = data(:, 1:m)';
y = data(:, m+1)';
y(y==0) = -1;
end